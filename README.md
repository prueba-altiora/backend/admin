# Admin

_Proyecto para la administración de clientes y articulos_
_La URL para poder ver las Apis documentadas al momento de correr el proyecto es:_
```sh
   http://localhost:9002/swagger-ui.html#
   ```

## Comenzando 🚀

_Estas instrucciones permitirán obtener una copia del proyecto en funcionamiento en su máquina local para propósitos poder realizar la revisión._
1. Clonar el repositorio
```sh
   git clone https://gitlab.com/prueba-altiora/backend/admin.git
   ```

### Pre-requisitos 📋

_Que cosas que se necesitan para correr el software_

1. Crear una base de datos PostgreSql con el nombre de admin.
```
Create database admin;
```

2. En el archivo application-dev.yaml, cambiar las credenciales (username,password) para que pueda conectar a su base de datos.
```
spring:
  datasource:
    url: jdbc:postgresql://localhost/admin
    username: postgres
    password: root
  jpa:
    database: postgresql
    hibernate:
      ddl-auto: update
```
3. Tener disponible el puerto 9002, ya que la aplicación correra en ese puerto, o en el archivo application.yaml cambiar el puerto en el que desea que corra la aplicación
```
server:
  port: 9002
```
4. Tener instalado Spring Tool Suit o Visual Studio Code para poder correr la aplicación.
5. Tener instalado java jdk8. 
6. Descargar las dependencias maven que se encuentran en el archio pom.xml del proyecto.
```
Esto se realiza haciendo un UpdateProject.
```

## Autor ✒️


* **Jefferson Esteban Yaguana Montero** 


