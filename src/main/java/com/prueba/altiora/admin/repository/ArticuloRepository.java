package com.prueba.altiora.admin.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.prueba.altiora.admin.model.Articulo;

@Repository
public interface ArticuloRepository extends JpaRepository<Articulo, Long>{
	
	List<Articulo> findByCodigo(String codigo);

}
