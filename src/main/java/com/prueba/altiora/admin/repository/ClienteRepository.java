package com.prueba.altiora.admin.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.prueba.altiora.admin.model.Cliente;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, String> {

}
