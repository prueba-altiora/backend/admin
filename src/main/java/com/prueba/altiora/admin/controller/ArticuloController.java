package com.prueba.altiora.admin.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.prueba.altiora.admin.model.Articulo;
import com.prueba.altiora.admin.service.imp.ArticuloServiceImp;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponse;

@RestController
@RequestMapping(value = "/articulo")
@Api(tags = "Administracion de Artículos")
public class ArticuloController {
	
	@Autowired
	private ArticuloServiceImp articuloService;
	
	@GetMapping("/listar")
	@ApiOperation(value = "Obtener todos los artículos registrados", notes = "<b>Ejemplo de envío</b></br>URL -> localhost:9002/articulo/listar")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Listado de los artículos registrados") })
	public List<Articulo> listarArticulos(){
		return articuloService.listarArticulos();
	}
	
	@PostMapping ("/crear")
	@ApiOperation(value = "Registro de un artículo")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Artículo registrado"),
			@ApiResponse(code = 409, message = "El artículo ya existe con el código ingresado."), })
	public Articulo crearArticulo(@Valid @RequestBody Articulo articulo){
		return articuloService.crearArticulo(articulo);
	}
	
	@PutMapping ("/actualizar")
	@ApiOperation(value = "Actualización de un artículo")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Artículo actualizado"),
			@ApiResponse(code = 404, message = "No se encontró el artículo para ser editado."), })
	public Articulo actualizarArticulo(@Valid @RequestBody Articulo articulo){
		return articuloService.actualizarArticulo(articulo);
	}
	
	@DeleteMapping("/eliminar/{id}")
	@ApiOperation(value = "Elimincación de un artículo")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Artículo eliminado"),
			@ApiResponse(code = 404, message = "No se encontró el artículo para ser eliminado."), })
	public void eliminarArticulo(@PathVariable Long id){
		articuloService.eliminarArticulo(id);
	}
	
	@GetMapping("/obtener/{id}")
	@ApiOperation(value = "Búsqueda de un artículo por id")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Artículo encontrado"),
			@ApiResponse(code = 404, message = "No se encontró el artículo."), })
	public Articulo ObtenerArticuloPorId(@PathVariable Long id){
		return articuloService.obtenerClientePorId(id);
	}
	
	@GetMapping("/obtener/stock/{idArticulo}")
	@ApiOperation(value = "Obtener Stock de un artículo")
	@ApiResponses(value = {@ApiResponse(code = 200, message = "Stock del Artículo"),
			@ApiResponse(code = 404, message = "No se encontró el artículo para ser consultado su stock")})
	public Long obtenerStockArticulo(@PathVariable Long idArticulo) {
		return articuloService.consultarStockArticulo(idArticulo);
	}

}
