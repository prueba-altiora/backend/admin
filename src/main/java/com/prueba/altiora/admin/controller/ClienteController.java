package com.prueba.altiora.admin.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.prueba.altiora.admin.model.Cliente;
import com.prueba.altiora.admin.service.imp.ClienteServiceImp;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(value = "/cliente")
@Api(tags = "Administracion de Clientes")
public class ClienteController {
	
	@Autowired
	private ClienteServiceImp clienteService;
	
	@GetMapping("/listar")
	@ApiOperation(value = "Obtener todos los clientes registrados", notes = "<b>Ejemplo de envío</b></br>URL -> localhost:9002/articulo/listar")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Listado de los clientes registrados") })
	public List<Cliente> listarClientes(){
		return clienteService.listarClientes();
	}
	
	@PostMapping ("/crear")
	@ApiOperation(value = "Registro de un cliente")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Cliente registrado"),
			@ApiResponse(code = 409, message = "El cliente ya existe con la identificación ingresada."), })
	public Cliente crearCliente(@Valid @RequestBody Cliente cliente){
		return clienteService.crearCliente(cliente);
	}
	
	@PutMapping ("/actualizar")
	@ApiOperation(value = "Actualización de un cliente")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Cliente actualizado"),
			@ApiResponse(code = 404, message = "No se encontró el cliente para ser editado."), })
	public Cliente actualizarCliente(@Valid @RequestBody Cliente cliente){
		return clienteService.actualizarCliente(cliente);
	}
	
	@DeleteMapping("/eliminar/{identificacion}")
	@ApiOperation(value = "Eliminación de un cliente")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Cliente eliminado"),
			@ApiResponse(code = 404, message = "No se encontró el cliente para ser eliminado."), })
	public void eliminarCliente(@PathVariable String identificacion){
		clienteService.eliminarCliente(identificacion);
	}
	
	
	@GetMapping("/obtener/{identificacion}")
	@ApiOperation(value = "Búsqueda de un cliente por identificacion")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Cliente encontrado"),
			@ApiResponse(code = 404, message = "No se encontró el cliente."), })
	public Cliente obtenerClientePorId(@PathVariable String identificacion){
		return clienteService.obtenerClientePorId(identificacion);
	}

}
