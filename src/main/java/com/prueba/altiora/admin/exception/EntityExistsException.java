package com.prueba.altiora.admin.exception;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

import org.apache.commons.lang3.StringUtils;

/**
 * EntityExistsException
 */
public class EntityExistsException extends RuntimeException {
	/**
	*
	*/
	private static final long serialVersionUID = 1L;

	private String mensajeSistema;

	public EntityExistsException(Class<?> _class, String param, Object valorParam) {
		super("Ya existe una entidad tipo ".concat(
				_class.getSimpleName().concat(" con valor ").concat(param.concat("=").concat(valorParam.toString()))));

	}

	public EntityExistsException(String mensajeSistema, Class<?> clazz, String... searchParamsMap) {
		super(EntityExistsException.generateMessage(clazz.getSimpleName(),
				toMap(String.class, String.class, searchParamsMap)));
		this.mensajeSistema = mensajeSistema;
	}

	private static String generateMessage(String entity, Map<String, String> searchParams) {
		return "Ya se encuentra registrado en la tabla " + StringUtils.capitalize(entity) + " para los parámetros "
				+ searchParams;
	}

	private static <K, V> Map<K, V> toMap(Class<K> keyType, Class<V> valueType, String... entries) {
		if (entries.length % 2 == 1)
			throw new IllegalArgumentException("Invalid entries");
		return IntStream.range(0, entries.length / 2).map(i -> i * 2).collect(HashMap::new,
				(m, i) -> m.put(keyType.cast(entries[i]), valueType.cast(entries[i + 1])), Map::putAll);
	}

	public String getMensajeSistema() {
		return mensajeSistema;
	}

	public void setMensajeSistema(String mensajeSistema) {
		this.mensajeSistema = mensajeSistema;
	}

}

