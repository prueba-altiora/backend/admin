package com.prueba.altiora.admin.exception;

public class NotStoreException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String mensajeSistema;
	
	public NotStoreException(String mensaje,String mensajeSistema) {
		super(mensaje);
		this.mensajeSistema= mensajeSistema;
	}

	public String getMensajeSistema() {
		return mensajeSistema;
	}

	public void setMensajeSistema(String mensajeSistema) {
		this.mensajeSistema = mensajeSistema;
	}
	
	

}
