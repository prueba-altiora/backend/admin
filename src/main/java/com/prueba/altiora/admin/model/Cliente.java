package com.prueba.altiora.admin.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@Entity
@Table(name = "cliente")
public class Cliente {

	@NotNull(message = "La identificación del cliente no puede ser null")
	@NotEmpty(message = "La identificación del cliente es obligatoria")
	@Size(min = 10, max = 13, message = "La identificación debe ser de 10 dígitos si es cédula o 13 dígitos si es RUC")
	@ApiModelProperty(value = "Identificación del cliente a registrarse.", required = true, dataType = "String")
	@Id
	@Column(name = "identificacion", length = 13, nullable = false, unique = true)
	private String identificacion;
	
	@NotNull(message = "Los nombres del cliente no pueden ser null")
	@NotEmpty(message = "Los nombres del cliente son obligatorios")
	@Size(min = 3, max = 100, message = "Los nombres del cliente no deben superar los 100 caracteres")
	@ApiModelProperty(value = "Nombres del cliente a registrarse.", required = true, dataType = "String")
	@Column(name = "nombres", length = 100, nullable = false)	
	private String nombres;
	
	@NotNull(message = "Los apellidos del cliente no pueden ser null")
	@NotEmpty(message = "Los apellidos del cliente son obligatoris")
	@Size(min = 3, max = 100, message = "Los apellidos del cliente no deben superar los 100 caracteres")
	@ApiModelProperty(value = "Apellidos del cliente a registrarse.", required = true, dataType = "String")
	@Column(name = "apellidos", length = 100, nullable = false)
	private String apellidos;
	
}
