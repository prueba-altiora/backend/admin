package com.prueba.altiora.admin.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@Entity
@Table(name = "articulo")
public class Articulo {
	@ApiModelProperty(value = "Id del artículo, este id es Autoincremental.", dataType = "Long")
	@Id
	@GeneratedValue(strategy =GenerationType.IDENTITY)
	private Long idArticulo;
	
	@NotNull(message = "El código del artículo no puede ser null")
	@NotEmpty(message = "El código del artículo es obligatorio")
	@Size(min = 3, max = 25, message = "El código del artículo no debe superar los 25 caracteres")
	@ApiModelProperty(value = "Código del artículo a registrarse.", required = true, dataType = "String",notes = "Este código es único para cada artículo")
	@Column(name = "codigo", length = 25, nullable = false)
	private String codigo;
	
	@NotNull(message = "El nombre del artículo no puede ser null")
	@NotEmpty(message = "El nombre del artículo es obligatorio")
	@Size(min = 3, max = 100, message = "El nombre del artículo no debe superar los 100 caracteres")
	@ApiModelProperty(value = "Nombre del artículo a registrarse.", required = true, dataType = "String")
	@Column(name = "nombre", length = 100, nullable = false)
	private String nombre;
	
	@NotNull(message = "El precio del artículo no puede ser null")
	@Digits(integer = 14, fraction = 6,message = "El precio unitario del artículo puede tener 14 dígitos para el entero y 6 dígitos para los decimales")
	@ApiModelProperty(value = "Precio unitario del artículo a registrarse.", required = true, dataType = "BigDecimal")
	@Column(name = "precioUnitario", nullable = false)
	private BigDecimal precioUnitario;
	
	@ApiModelProperty(value = "Stock que posee el artículo",required = true, dataType = "Long")
	@Column (name = "stock",nullable = true)
	private Long stock;
}
