package com.prueba.altiora.admin.service;

public interface StockArticuloService {

	Long consultarStockArticulo(Long idArticulo);
	
}
