package com.prueba.altiora.admin.service;

public interface EliminarClienteService {
	
	void eliminarCliente(String identificacion);

}
