package com.prueba.altiora.admin.service.imp;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prueba.altiora.admin.exception.EntityExistsException;
import com.prueba.altiora.admin.exception.EntityNotFoundException;
import com.prueba.altiora.admin.model.Articulo;
import com.prueba.altiora.admin.repository.ArticuloRepository;
import com.prueba.altiora.admin.service.ActualizarArticuloService;
import com.prueba.altiora.admin.service.ArticuloPorIdService;
import com.prueba.altiora.admin.service.CrearArticuloService;
import com.prueba.altiora.admin.service.EliminarArticuloService;
import com.prueba.altiora.admin.service.ListarArticuloService;
import com.prueba.altiora.admin.service.StockArticuloService;
import com.prueba.altiora.admin.service.model.ArticuloService;

@Service
public class ArticuloServiceImp implements CrearArticuloService,ActualizarArticuloService,EliminarArticuloService,ListarArticuloService,ArticuloPorIdService, StockArticuloService {
	
	@Autowired
	ArticuloRepository articuloRepository;
	
	@Autowired
	ArticuloService articuloService;

	@Override
	public Articulo crearArticulo(Articulo articulo) {
		
		if (articuloService.obtenerArticuloPorCodigo(articulo.getCodigo()).isPresent()) {
			throw new EntityExistsException("El articulo ya se encuentra registrado con el código ".concat(articulo.getCodigo()), Articulo.class, "Código ",articulo.getCodigo());
		}		
		return articuloRepository.save(articulo);
	}

	@Override
	public Articulo actualizarArticulo(Articulo articulo) {
		if(!articuloRepository.findById(articulo.getIdArticulo()).isPresent()) {
			throw new EntityNotFoundException("El articulo no se encuentra registrado.", Articulo.class, "Id Articulo",articulo.getIdArticulo().toString());
		}
		return articuloRepository.save(articulo);
	}

	@Override
	public void eliminarArticulo(Long id) {
		if(!articuloRepository.findById(id).isPresent()) {
			throw new EntityNotFoundException("El articulo no se encuentra registrado.", Articulo.class, "Id Articulo",id.toString());
		}
		articuloRepository.deleteById(id);		
	}

	@Override
	public List<Articulo> listarArticulos() {
		return articuloRepository.findAll();
	}

	@Override
	public Articulo obtenerClientePorId(Long idArticulo) {
		
		Optional<Articulo> articuloOptional =articuloRepository.findById(idArticulo);
		if(!articuloOptional.isPresent()) {
			throw new EntityNotFoundException("El articulo no se encuentra registrado.", Articulo.class, "Id Articulo",idArticulo.toString());
		}
		return articuloOptional.get();	
	}

	@Override
	public Long consultarStockArticulo(Long idArticulo) {
		
		Optional<Articulo> articuloOptional = articuloRepository.findById(idArticulo);
		if(!articuloOptional.isPresent()) {
			throw new EntityNotFoundException("El articulo no se encuentra registrado.", Articulo.class, "Id Artículo", idArticulo.toString());
		}
		
		return articuloOptional.get().getStock();
	}
	
	
	
	

}
