package com.prueba.altiora.admin.service;

import java.util.List;

import com.prueba.altiora.admin.model.Articulo;

public interface ListarArticuloService {

	List<Articulo> listarArticulos();
}
