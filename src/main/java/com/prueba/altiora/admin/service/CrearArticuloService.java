package com.prueba.altiora.admin.service;

import com.prueba.altiora.admin.model.Articulo;

public interface CrearArticuloService {
	
	Articulo crearArticulo(Articulo articulo);

}
