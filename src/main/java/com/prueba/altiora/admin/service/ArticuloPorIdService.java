package com.prueba.altiora.admin.service;

import com.prueba.altiora.admin.model.Articulo;

public interface ArticuloPorIdService {

	Articulo obtenerClientePorId(Long idArticulo);
}
