package com.prueba.altiora.admin.service;

import com.prueba.altiora.admin.model.Cliente;

public interface CrearClienteService {
	
	Cliente crearCliente(Cliente cliente);

}
