package com.prueba.altiora.admin.service.model;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prueba.altiora.admin.model.Articulo;
import com.prueba.altiora.admin.repository.ArticuloRepository;

@Service
public class ArticuloService {
	
	@Autowired
	ArticuloRepository articuloRepository;
	
	public Optional<Articulo> obtenerArticuloPorCodigo(String codigo){
		List<Articulo> articulos=articuloRepository.findByCodigo(codigo);
		if (articulos == null || articulos.isEmpty()) {
			return Optional.empty();
		}

		return Optional.of(articulos.get(0));
	}

}
