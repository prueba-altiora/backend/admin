package com.prueba.altiora.admin.service;

import java.util.List;

import com.prueba.altiora.admin.model.Cliente;

public interface ListarClienteService {

	List<Cliente> listarClientes();
}
