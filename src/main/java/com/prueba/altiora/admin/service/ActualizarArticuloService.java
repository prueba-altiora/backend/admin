package com.prueba.altiora.admin.service;

import com.prueba.altiora.admin.model.Articulo;

public interface ActualizarArticuloService {

	Articulo actualizarArticulo(Articulo articulo);
}
