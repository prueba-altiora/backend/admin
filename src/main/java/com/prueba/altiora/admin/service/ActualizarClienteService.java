package com.prueba.altiora.admin.service;

import com.prueba.altiora.admin.model.Cliente;

public interface ActualizarClienteService {

	Cliente actualizarCliente(Cliente cliente);
}
