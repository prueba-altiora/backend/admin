package com.prueba.altiora.admin.service;

public interface EliminarArticuloService {
	
	void eliminarArticulo(Long id);

}
