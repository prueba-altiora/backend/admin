package com.prueba.altiora.admin.service.imp;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prueba.altiora.admin.exception.EntityExistsException;
import com.prueba.altiora.admin.exception.EntityNotFoundException;
import com.prueba.altiora.admin.model.Cliente;
import com.prueba.altiora.admin.repository.ClienteRepository;
import com.prueba.altiora.admin.service.ActualizarClienteService;
import com.prueba.altiora.admin.service.ClientePorIdService;
import com.prueba.altiora.admin.service.CrearClienteService;
import com.prueba.altiora.admin.service.EliminarClienteService;
import com.prueba.altiora.admin.service.ListarClienteService;

@Service
public class ClienteServiceImp implements CrearClienteService,ActualizarClienteService,EliminarClienteService,ListarClienteService,ClientePorIdService {
	
	@Autowired
	ClienteRepository clienteRepository;

	@Override
	public Cliente crearCliente(Cliente cliente) {
		
		if (clienteRepository.findById(cliente.getIdentificacion()).isPresent()) {
			throw new EntityExistsException("El cliente ya se encuentra registrado.", Cliente.class, "Identificación",cliente.getIdentificacion());
		}		
		return clienteRepository.save(cliente);
	}

	@Override
	public Cliente actualizarCliente(Cliente cliente) {
		if(!clienteRepository.findById(cliente.getIdentificacion()).isPresent()) {
			throw new EntityNotFoundException("El cliente no se encuentra registrado.", Cliente.class, "Identificación",cliente.getIdentificacion());
		}
		return clienteRepository.save(cliente);
	}

	@Override
	public void eliminarCliente(String identificacion) {
		if(!clienteRepository.findById(identificacion).isPresent()) {
			throw new EntityNotFoundException("El cliente no se encuentra registrado.", Cliente.class, "Identificación",identificacion);
		}
		clienteRepository.deleteById(identificacion);		
	}

	@Override
	public List<Cliente> listarClientes() {
		return clienteRepository.findAll();
	}

	@Override
	public Cliente obtenerClientePorId(String identificacion) {
		Optional<Cliente> clienteOptional =clienteRepository.findById(identificacion);
		if(!clienteOptional.isPresent()) {
			throw new EntityNotFoundException("El cliente no se encuentra registrado.", Cliente.class, "Identificación",identificacion);
		}
		return clienteOptional.get();
	}
	
	
	
	

}
